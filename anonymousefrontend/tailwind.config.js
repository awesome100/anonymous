module.exports = {
  content: ["./src/**/*.{html,js}", "./node_modules/flowbite/**/*.js"
],
darkMode: 'class',

  theme: {
    extend: {
      colors: {
        kaffirpurple: '#26066F',
        kaffirbtn:'#F72585',
        kaffirCard:'#DADADA'
      },
    },
  },
  plugins: [
    require('flowbite/plugin')

  ],
}
