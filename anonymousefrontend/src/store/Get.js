import { useEffect, useState } from "react"
const axios = require('axios');

const useGet = (url) =>{
    const [data, setData] = useState(null);
   
     useEffect(()=>{
            axios.get(url)
            .then((data)=>{
                setData(data.data)
            }) 
            .catch((error)=> {return error})
        
     },[url])
     return {data}
}
export default useGet;