import store from "./index"
import { useState } from 'react';
import Notification from '../components/notification';

const UseNewLink=(e)=>{
    
    const [Loading, setLoading] = useState(false);
    const [NotifySuccess, setNotifySuccess]= useState({status:false, message:""})
    const [NotifyFailed, setNotifyFailed]= useState({status:false, message:""})
    store.post('post',{
        endpoint:"generate-anonymus-link",
        details:{}
    }).then(
        (data)=>{
            var user_details = data.data.data;
                var result =  encodeURIComponent(JSON.stringify(user_details));
                var user_token= data.data.token;
                localStorage.setItem('kaffir_token', user_token);
                localStorage.setItem('kaffir_user', result);
                setLoading(false);
                
        }
    ).catch((error) => {
        setLoading(false);

            var error_message = error.response.data.message;
                setNotifyFailed(
                    {
                        ...NotifyFailed,
                        status:true,
                        message:error_message
                    })
                    setTimeout(() => {
                        setNotifyFailed(
                            {
                                ...NotifyFailed,
                                status:false,
                            })
                       }, 3000);
           
            setLoading(false);
        });
}
export default NewLink;