import "../../App.css";
import React, { useState } from "react";
import store from "../../store/index";
import { Link, useParams, useNavigate } from "react-router-dom";

const UseprofileHooks = ()=>{
    let navigate = useNavigate()
    const [modalOpen, setmodalOpen] = useState("hidden");
    const [Loading, setLoading] = useState(false);
    const [NotifySuccess, setNotifySuccess] = useState({ status: false, message: "" });
    const [NotifyFailed, setNotifyFailed] = useState({ status: false, message: "" });
    const [Username, setUsername] = useState({ username: "" });

   

    const copyToClipboard = () => {
        var copyText = document.getElementById("anonLink");
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        setTimeout(async () => await window.navigator.clipboard.writeText(copyText.value), 2000);
        alert("Copied the text: " + copyText.value);
    };
    const NewLink = () => {
        store
            .post({
                endpoint: "generate-anonymous-link",
                details: {},
            })
            .then((data) => {
                var user_details = data.data.data;
                var result = encodeURIComponent(JSON.stringify(user_details));
                localStorage.setItem("user", result);
                // navigate('/'  + user_details.username)
                window.location.href="/" + user_details.username
                setLoading(false);
            })
            .catch((error) => {
                setLoading(false);

                var error_message = error.response.data.message;
                setNotifyFailed({
                    ...NotifyFailed,
                    status: true,
                    message: error_message,
                });
                setTimeout(() => {
                    setNotifyFailed({
                        ...NotifyFailed,
                        status: false,
                    });
                }, 3000);

                setLoading(false);
            });
    };
    const UpdateUsername = () => {
        setLoading(true);
        store
            .post({
                endpoint: "update-details",
                details: Username,
            })
            .then((data) => {
                var user_details = data.data.data;
                var result = encodeURIComponent(JSON.stringify(user_details));
                localStorage.setItem("user", result);
                setLoading(false);
                window.location.href="/" + user_details.username

            })
            .catch((error) => {
                setLoading(false);

                var error_message = error.response.data.message;
                setNotifyFailed({
                    ...NotifyFailed,
                    status: true,
                    message: error_message,
                });
                setTimeout(() => {
                    setNotifyFailed({
                        ...NotifyFailed,
                        status: false,
                    });
                }, 3000);

                setLoading(false);
            });
    };
    const logout = () => {
        localStorage.removeItem("kaffir_user");
        localStorage.removeItem("kaffir_token");
        navigate("/signin")
    };
    return{
        modalOpen,Loading,NotifySuccess,copyToClipboard,NewLink ,UpdateUsername,logout,setmodalOpen,setNotifySuccess,setNotifyFailed
    }
}
export default UseprofileHooks;