import { useEffect, useState } from "react"
import { useRef } from "react/cjs/react.production.min";
const axios = require('axios');

const usePost = (endpoint, details) =>{
    const [data, setData] = useState(null);
    const ismounted = useRef()
     useEffect(()=>{
       if (ismounted.current) return
        axios({
            method: 'post',
            url: endpoint,
            data: details
          })
        .then((data)=>{
            setData(data.data)
        }) 
        .catch((error)=>{});
        ismounted.current=true

     },[endpoint])
     return {data}
}
export default usePost;